### Laravel Migrasi
- setting .env
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=belajar_laravel
DB_USERNAME=root
DB_PASSWORD=root
```

- membuat file migrasi
```
$ php artisan make:migration create_mahasiswa_table
```

- ubah file migrasi tsb
```
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('nim');
            $table->text('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa');
    }
}


/*
# tipe column migrasi
$table->increments('id');
$table->bigIncrements('id');
$table->bigInteger('votes');

$table->boolean('confirmed');

$table->char('name', 100);

$table->date('created_at');
$table->dateTime('created_at');

$table->decimal('amount',8,2);

$table->integer('votes');

$table->string('name', 100);
$table->longText('description');
$table->text('description');

$table->year('birth_year');
*/
```

- menjalankan migrasi
```
$ php artisan migrate
```

### Perubahan Database lewat Migrasi
- ubah table
```
Schema::rename('nama_table_yang_ingin_di_rename', 'nama_baru');
```

- hapus table
```
Schema::drop('nama_table');

atau

Schema::dropIfExists('nama_table');
```

- rollback database
```
# 1 langkah
$ php artisan migrate:rollback

# 5 langkah
$ php artisan miograte:rollback --step=5
```
