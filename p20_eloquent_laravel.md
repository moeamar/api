### Eloquent Database Laravel
Eloquent adalah sebuah fitur untuk mengelola data
yang ada pada database dengan sangat mudah.


Eloquent ORM menyediakan fungsi2 active record atau fungsi2 query sql untuk mengelola data pada database.

- persiapan database
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=belajar_laravel
DB_USERNAME=root
DB_PASSWORD=root
```

- buat model dan migrasi
```
# model aja
$ php artisan make:model Pegawai

# model & migrasi
$ php artisan make:model Pegawai -m
$ php artisan make:model Pegawai --migration
```


- ubah file migrasi
```
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->text('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
```

- menjalankan migrasi
```
$ php artisan migrate
```

### Menyiapkan data seeder

- membuat faker & seeder
```
$ php artisan make:seed PegawaiSeeder
```

- ubah file seeder
```
<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonesia
        $faker = Faker::create('id_ID');

        // membuat data dummy sebanyak 10 record
        for($x = 1; $x <= 10; $x++){

        	// insert data dummy pegawai dengan faker
        	DB::table('pegawai')->insert([
        		'nama' => $faker->name,
        		'alamat' => $faker->address,
        	]);

        }

    }
}
```

- menjalankan db:seeder
```
$ php artisan db:seed --class=PegawaiSeeder
```

### Cara menggunakan Eloquent Laravel
- ubah data model Pegawai
```
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = "pegawai";
}
```

- ubah route
```
Route::get('/pegawai', 'PegawaiController@index');
```

- membuat controller
```
$ php artisan make:controller PegawaiController
```

- ubah file controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// panggil model pegawai
use App\Pegawai;


class PegawaiController extends Controller
{
    public function index()
    {
    	// mengambil data pegawai
    	$pegawai = Pegawai::all();

    	// mengirim data pegawai ke view pegawai
    	return view('pegawai', ['pegawai' => $pegawai]);
    }

}
```

- buat file view
```
<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #20 : Eloquent Laravel</title>
</head>
<body>

<h1>Data Pegawai</h1>
<h3>www.malasngoding.com</h3>

<ul>
	@foreach($pegawai as $p)
		<li>{{ "Nama : ". $p->nama . ' | Alamat : ' . $p->alamat }}</li>
	@endforeach
</ul>

</body>
</html>
```

- jalankan server
```
$ php artisan serve
```

### Eloquent Lanjutan
- ambil first
```
public function index() { 

	// mengambil data pegawai yang pertama
	$pegawai = Pegawai::first(); 

	// mengirim data pegawai ke view pegawai 
	return view('pegawai', ['pegawai' => $pegawai]); 

}
```

- find dengan id
```
public function index() { 

	// mengambil data pegawai yang id nya 1
	$pegawai = Pegawai::find(1); 

	// mengirim data pegawai ke view pegawai 
	return view('pegawai', ['pegawai' => $pegawai]); 

}
```

- cari dengan where
```
public function index() { 

	// mengambil data pegawai yang bernama Jamal Uwais
	$pegawai = Pegawai::where('nama', 'Jamal Uwais')->get(); 

	// mengirim data pegawai ke view pegawai 
	return view('pegawai', ['pegawai' => $pegawai]); 

}
```

- where lanjutan
```
# mengambil data pegawai yang bernama Jamal Uwais
$pegawai = Pegawai::where('nama', '=' , 'Jamal Uwais')->get(); 


# mengambil data pegawai yang bernama Jamal Uwais
$pegawai = Pegawai::where('nama', '=' , 'Jamal Uwais')->get(); 


# mengambil data pegawai yang id nya lebih besar sama dengan 10 
$pegawai = Pegawai::where('id', '>=' , 10)->get();


# mengambil dengan like
# mengambil data pegawai yang di namanya ada huruf a 
$pegawai = Pegawai::where('nama', 'like' , '%a%')->get();
```

- membuat pagination
```
# menampilkan 10 data pegawai per halaman
$pegawai = Pegawai::paginate(10);
```