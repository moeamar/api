### Softdelete Laravel 
- buat file migrate
```
$ php artisan make:migration mahasiswa_create_deleted_at
```


- ubah file migrate
```
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MahasiswaCreateDeletedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mahasiswa', function (Blueprint $table) {
            $table->dateTime('deleted_at')->after('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswa', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}

```

- jalankan migrate
```
$ php artisan migrate
```

- ubah model
```
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Guru extends Model
{
	use SoftDeletes;

    protected $table = "guru";
   	protected $dates = ['deleted_at'];
}
```

- ubah view index, tambahkan route trash
```
<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #22 : Soft Deletes Laravel</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>

	<div class="container">

		<div class="card mt-5">
			<div class="card-header text-center">
				Data Guru | <a href="https://www.malasngoding.com/laravel">www.malasngoding.com</a>
			</div>
			<div class="card-body">

				<a href="/guru" class="btn btn-sm btn-primary">Data Guru</a>
				|
				<a href="/guru/trash">Tong Sampah</a>

				<br/>
				<br/>

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Umur</th>
							<th width="1%">OPSI</th>
						</tr>
					</thead>
					<tbody>
						@foreach($guru as $g)
						<tr>
							<td>{{ $g->nama }}</td>
							<td>{{ $g->umur }}</td>
							<td><a href="/guru/hapus/{{ $g->id }}" class="btn btn-danger btn-sm">Hapus</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>
```

### Menampilkan Data Trash
- buat route trash
```
Route::get('/guru/trash', 'GuruController@trash');
```

- ubah controller dh fungsi trashed
```
	public function trash(){
    	$mahasiswa = Mahasiswa::onlyTrashed()->get();
    	return view('eloquent/mahasiswa_trash', ['mahasiswa' => $mahasiswa]);
    }
```

- buat view trashed
```
<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #22 : Soft Deletes Laravel</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>

	<div class="container">

		<div class="card mt-5">
			<div class="card-header text-center">
				Data Guru | <a href="https://www.malasngoding.com/laravel">www.malasngoding.com</a>
			</div>
			<div class="card-body">

				<a href="/guru">Data Guru</a>
				|
				<a href="/guru/trash" class="btn btn-sm btn-primary">Tong Sampah</a>

				<br/>
				<br/>

				<a href="/guru/kembalikan_semua">Kembalikan Semua</a>
				|
				<a href="/guru/hapus_permanen_semua">Hapus Permanen Semua</a>
				<br/>
				<br/>

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Umur</th>
							<th width="30%">OPSI</th>
						</tr>
					</thead>
					<tbody>
						@foreach($guru as $g)
						<tr>
							<td>{{ $g->nama }}</td>
							<td>{{ $g->umur }}</td>
							<td>
								<a href="/guru/kembalikan/{{ $g->id }}" class="btn btn-success btn-sm">Restore</a>
								<a href="/guru/hapus_permanen/{{ $g->id }}" class="btn btn-danger btn-sm">Hapus Permanen</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</body>
</html>
```

### Restore Data yg Dihapus
- buat route kembalikan/{id}
```
Route::get('/guru/kembalikan/{id}', 'GuruController@kembalikan');
```

- buat fungsi controller 
```
// restore data guru yang dihapus
public function kembalikan($id)
{
    	$guru = Guru::onlyTrashed()->where('id',$id);
    	$guru->restore();
    	return redirect('/guru/trash');
}
```

### Restore Data Semua
- buat route kembalikan_semua
```
Route::get('/guru/kembalikan_semua', 'GuruController@kembalikan_semua');
```

- buat fungsi controller
```
// restore semua data guru yang sudah dihapus
public function kembalikan_semua()
{
    		
    	$guru = Guru::onlyTrashed();
    	$guru->restore();

    	return redirect('/guru/trash');
}
```

### Hapus Permanen ID
- buat route hapus_permanen/{id}
```
Route::get('/guru/hapus_permanen/{id}', 'GuruController@hapus_pemanen');
```
- buat controller fungsi
```
public function hapus_permanen($id)
{
	// hapus permanen data guru
	$guru = Guru::onlyTrashed()->where('id',$id);
	$guru->forceDelete();

	return redirect('/guru/trash');
}
```


### Hapus Permanen Semua
- buat route hapus_permanen_semua
```
Route::get('/guru/hapus_permanen_semua', 'GuruController@hapus_pemanen_semua');
```

- buat controller fungsi
```
public function hapus_permanen_semua(){
	// hapus permanen semua data guru yang sudah dihapus
	$guru = Guru::onlyTrashed();
	$guru->forceDelete();

	return redirect('/guru/trash');
}
```

