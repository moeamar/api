### Export Excell Laravel
- install library
```
$ composer require maatwebsite/excel
```

- ubah file config/app.php
```
'providers' => [
    ...
    Maatwebsite\Excel\ExcelServiceProvider::class,
]

'aliases' => [
    ...
    'Excel' => Maatwebsite\Excel\Facades\Excel::class,
]
```

- buat peraturan excell di laravel
```
$php artisan vendor:publish --provider="Maatwebsite\Excel\ExcelServiceProvider"

# maka file /config/excel.php akan terbuat
```

- buat class Export
```
$ php artisan make:export SiswaExport --model=Siswa

# maka akan terbuat file app/Exports/
```

- ubah file app/Exports/
```
<?php

namespace App\Exports;

use App\Siswa;
use Maatwebsite\Excel\Concerns\FromCollection;

class SiswaExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Siswa::all();
    }
}
```

- buat route
```
Route::get('/siswa', 'SiswaController@index');
Route::get('/siswa/export_excel', 'SiswaController@export_excel');
```

- buat controller
```
$ php artisan make:controller SiswaController
```

- ubah controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Siswa;

use App\Exports\SiswaExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class SiswaController extends Controller
{
	public function index(){
		$siswa = Siswa::all();
		return view('siswa',['siswa'=>$siswa]);
	}

	public function export_excel(){
		return Excel::download(new SiswaExport, 'siswa.xlsx');
	}
}
```

### Buat View Laravel
- buat siswa
```
<!DOCTYPE html>
<html>
<head>
	<title>Export Laporan Excel Pada Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

	<div class="container">
		<center>
			<h4>Export Laporan Excel Pada Laravel</h4>
			<h5><a target="_blank" href="https://www.malasngoding.com/">www.malasngoding.com</a></h5>
		</center>
		
		<a href="/siswa/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>
		
		<table class='table table-bordered'>
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIS</th>
					<th>Alamat</th>
				</tr>
			</thead>
			<tbody>
				@php $i=1 @endphp
				@foreach($siswa as $s)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{$s->nama}}</td>
					<td>{{$s->nis}}</td>
					<td>{{$s->alamat}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</body>
</html>
```

- jalankan laravel
```
$ php artisan serve
```
