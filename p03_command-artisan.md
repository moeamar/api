### Command artisan laravel
- membuat controller
```
$ php artisan make:controller Contoh
```

- membuat model & migration
```
# membuat model
$ php artisan make:model Contoh -m

# meng-generate model
$ php artisan migrate

# membatalkan model rollback
$ php artisan migrate:rollback
```

- LINK: https://www.toptal.com/laravel/restful-laravel-api-tutorial


### Membuat table dan Migrasi
- membuat model  dan migrasi
```
$ php artisan make:model Article -m
```

- atur file migrate
```
public function up()
{
    Schema::create('articles', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title');
        $table->text('body');
        $table->timestamps();
    });
}
```
- migrate to database
```
$ php artisan migrate
```

- set fillable model Article
```
class Article extends Model
{
    protected $fillable = ['title', 'body'];
}
```

- buat seeder & faker
```
$ php artisan make:seeder ArticlesTableSeeder
```

-- ubah file seeder
```
class ArticlesTableSeeder extends Seeder
{
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Article::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Article::create([
                'title' => $faker->sentence,
                'body' => $faker->paragraph,
            ]);
        }
    }
}
```
- running file seeder
```
$ php artisan db:seed --class=ArticlesTableSeeder
```



### HTTP Status Code
```
200 - ok
201 - obj create
204 - no content
206 - partial content

400 - bad request
401 - unauthorized
403 - forbidden
404 - not found

500 - internal server error
503 - service unavaible
```