### Kirim Email Laravel
- set env
```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=xxxx@pandawa-mqzz.com
MAIL_PASSWORD=xxxxx
MAIL_ENCRYPTION=tls
```

- buat route
```
Route::get('/kirimemail','KirimEmailController@index');
```

- buat controller
```
$ php artisan make:controller KirimEmailController
```

- ubah file controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Mail\MalasngodingEmail;
use Illuminate\Support\Facades\Mail;

class KirimEmailController extends Controller
{
	public function index(){

		Mail::to("testing@malasngoding.com")->send(new MalasngodingEmail());

		return "Email telah dikirim";

	}

}
```

- buat class mailable
```
# letak file
app/Mail/MalasngodingEmail.php

# isi email
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MalasngodingEmail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct()
    {
        
    }

    
    public function build()
    {
       // tanpa attachment
       return $this->from('pengirim@malasngoding.com')
            ->view('email/emailku')
            ->with([
                'nama' => 'Diki Alfarabi Hadi',
                'website' => 'www.malasngoding.com',
            ]);

       // dengan attachment
        return $this->from('pengirim@malasngoding.com')
           ->view('emailku')
           ->with(
            [
                'nama' => 'Diki Alfarabi Hadi',
                'website' => 'www.malasngoding.com',
            ])
            ->attach(public_path('/hubungkan-ke-lokasi-file').'/demo.jpg', [
              'as' => 'demo.jpg',
              'mime' => 'image/jpeg',
            ]);
    }
}
```

- buat view template email
```
<h3>Halo, {{ $nama }} !</h3>
<p>{{ $website }}</p>

<p>Selamat datang di <a href="https://www.malasngoding.com/kirim-email-dengan-laravel/">www.malasngoding.com</a></p>
<p>Tutorial Laravel #35 : kirim email dengan laravel.</p>
```

- jalankan laravel
```
$ php artisan serve
```
