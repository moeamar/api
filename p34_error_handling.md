### Error Handling Laravel
- ubah envirotment
```
# jika mode development
APP_DEBUG=true

# jika mode produksi
APP_DEBUG=false

# hasil log kesalahan akan disimpan
storage/logs
```

- code error laravel
```
404 - Error Halaman tidak ditemukan
403 - Hak Akses Forbiden
500 - Error Server/Code
```

### Handle Error dari Script
- buat route
```
Route::get('/errorhandling/{nama}','ErrorHandlingController');
```

- buat controller
```
$ php artisan make:controller ErrorHandlingController
```

- ubah controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MalasngodingController extends Controller{

	public function index($nama){
		if($nama == "malasngoding"){
			return abort(403,'Anda tidak punya akses karena anda Malas Ngoding');
		}elseif($nama == "diki"){
			return "Halo, ".$nama;
		}else{
			return abort(404);
		}
	}

}
```

- panggil error view
```
abort(nomor_error)
```


### Mengubah Tampilan Error
- jalankan pushlish error
```
$ php artisan vendor:publish --tag=laravel-errors
```

- file error akan berada di
```
resource/views/errors
```
